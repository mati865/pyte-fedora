%global srcname pyte
%global sum Simple VTXXX-compatible linux terminal emulator

Name:           python-%{srcname}
Version:        0.8.0
Release:        1%{?dist}
Summary:        %{sum}

License:        LGPLv3
URL:            https://pypi.python.org/pypi/%{srcname}
# pythonhosted.org archive has broken tests
Source0:        https://github.com/selectel/%{srcname}/archive/%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python2-devel
BuildRequires:  python2-pytest-runner
BuildRequires:  python2-wcwidth
BuildRequires:  python3-devel
BuildRequires:  python3-pytest-runner
BuildRequires:  python3-wcwidth

%description
It's an in memory VTXXX-compatible terminal emulator.
XXX stands for a series of video terminals,
developed by DEC between 1970 and 1995.

%package -n python2-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python2-%{srcname}}
Requires:       python2-wcwidth

%description -n python2-%{srcname}
It's an in memory VTXXX-compatible terminal emulator.
XXX stands for a series of video terminals,
developed by DEC between 1970 and 1995.


%package -n python3-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python3-%{srcname}}
Requires:       python3-wcwidth

%description -n python3-%{srcname}
It's an in memory VTXXX-compatible terminal emulator.
XXX stands for a series of video terminals,
developed by DEC between 1970 and 1995.


%prep
%autosetup -n %{srcname}-%{version}

%build
%py2_build
%py3_build

%install
%py2_install
%py3_install

%check
%{__python2} setup.py test
%{__python3} setup.py test

%files -n python2-%{srcname}
%license LICENSE
%doc README
%{python2_sitelib}/*

%files -n python3-%{srcname}
%license LICENSE
%doc README
%{python3_sitelib}/*

%changelog
* Tue May 8 2018 Mateusz Mikuła <mati865 at gmail.com> - 0.8.0-1
- Initial packaging
